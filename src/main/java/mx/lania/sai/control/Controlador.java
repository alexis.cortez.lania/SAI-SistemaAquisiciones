package mx.lania.sai.control;

import java.util.List;
import mx.lania.sai.entidades.Participantes;
import mx.lania.sai.entidades.Agentecontratante;
import mx.lania.sai.oad.OadAgentecontratante;
import mx.lania.sai.oad.OadParticipantes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author PCEL
 */
@RestController
public class Controlador {
    
    @Autowired
    OadAgentecontratante oadAgente;
    
    @Autowired
    OadParticipantes oadParticipantes;
            
    @GetMapping("/participantes")
    public List<Participantes> getParticipantes() {
        return oadParticipantes.findAll();
    }
    
    @PostMapping("/participante")
    public Participantes crearParticipante(@RequestBody Participantes usr) {
        oadParticipantes.save(usr);
        return usr;
    }
    
    @PutMapping("/participantes/{id}")
    public Participantes actualizarParticipante(@RequestBody Participantes usr) {
        return null;
    }
    
    @GetMapping("/agente")
    public List<Agentecontratante> getEmpleados() {
        return oadAgente.findAll();
    }
    
    @GetMapping(value="/agente", params = {"nombre"})
    public List<Agentecontratante> buscarEmpleadosPorNombre(@RequestParam("nombre") String nombre) {
        return oadAgente.buscarPorNombre(nombre);
    }
    
    @GetMapping("/agente/{id}")
    public Agentecontratante getEmpleadoPorId(@PathVariable("id")Integer idAgente) {
        return oadAgente.getPorId(idAgente);
    }
}






