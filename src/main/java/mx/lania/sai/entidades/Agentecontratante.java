/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.lania.sai.entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author alexiscortez
 */
@Entity
@Table(name = "AGENTECONTRATANTE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Agentecontratante.findAll", query = "SELECT a FROM Agentecontratante a")
    , @NamedQuery(name = "Agentecontratante.findByIdagente", query = "SELECT a FROM Agentecontratante a WHERE a.idagente = :idagente")
    , @NamedQuery(name = "Agentecontratante.findByNombre", query = "SELECT a FROM Agentecontratante a WHERE a.nombre = :nombre")
    , @NamedQuery(name = "Agentecontratante.findByApellido", query = "SELECT a FROM Agentecontratante a WHERE a.apellido = :apellido")
    , @NamedQuery(name = "Agentecontratante.findByEmpresa", query = "SELECT a FROM Agentecontratante a WHERE a.empresa = :empresa")
    , @NamedQuery(name = "Agentecontratante.findByArea", query = "SELECT a FROM Agentecontratante a WHERE a.area = :area")})
public class Agentecontratante implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "IDAGENTE")
    private Integer idagente;
    @Size(max = 60)
    @Column(name = "NOMBRE")
    private String nombre;
    @Size(max = 60)
    @Column(name = "APELLIDO")
    private String apellido;
    @Size(max = 60)
    @Column(name = "EMPRESA")
    private String empresa;
    @Size(max = 60)
    @Column(name = "AREA")
    private String area;

    public Agentecontratante() {
    }

    public Agentecontratante(Integer idagente) {
        this.idagente = idagente;
    }

    public Integer getIdagente() {
        return idagente;
    }

    public void setIdagente(Integer idagente) {
        this.idagente = idagente;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idagente != null ? idagente.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Agentecontratante)) {
            return false;
        }
        Agentecontratante other = (Agentecontratante) object;
        if ((this.idagente == null && other.idagente != null) || (this.idagente != null && !this.idagente.equals(other.idagente))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.lania.sai.entidades.Agentecontratante[ idagente=" + idagente + " ]";
    }
    
}
