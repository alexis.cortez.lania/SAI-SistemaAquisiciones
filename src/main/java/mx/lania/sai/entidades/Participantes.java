/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.lania.sai.entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author alexiscortez
 */
@Entity
@Table(name = "PARTICIPANTES")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Participantes.findAll", query = "SELECT p FROM Participantes p")
    , @NamedQuery(name = "Participantes.findByIdparticipante", query = "SELECT p FROM Participantes p WHERE p.idparticipante = :idparticipante")
    , @NamedQuery(name = "Participantes.findByNombreEmpresa", query = "SELECT p FROM Participantes p WHERE p.nombreEmpresa = :nombreEmpresa")
    , @NamedQuery(name = "Participantes.findByDireccion", query = "SELECT p FROM Participantes p WHERE p.direccion = :direccion")
    , @NamedQuery(name = "Participantes.findByRepresentante", query = "SELECT p FROM Participantes p WHERE p.representante = :representante")})
public class Participantes implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "IDPARTICIPANTE")
    private Integer idparticipante;
    @Size(max = 60)
    @Column(name = "NOMBRE_EMPRESA")
    private String nombreEmpresa;
    @Size(max = 60)
    @Column(name = "DIRECCION")
    private String direccion;
    @Size(max = 60)
    @Column(name = "REPRESENTANTE")
    private String representante;

    public Participantes() {
    }

    public Participantes(Integer idparticipante) {
        this.idparticipante = idparticipante;
    }

    public Integer getIdparticipante() {
        return idparticipante;
    }

    public void setIdparticipante(Integer idparticipante) {
        this.idparticipante = idparticipante;
    }

    public String getNombreEmpresa() {
        return nombreEmpresa;
    }

    public void setNombreEmpresa(String nombreEmpresa) {
        this.nombreEmpresa = nombreEmpresa;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getRepresentante() {
        return representante;
    }

    public void setRepresentante(String representante) {
        this.representante = representante;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idparticipante != null ? idparticipante.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Participantes)) {
            return false;
        }
        Participantes other = (Participantes) object;
        if ((this.idparticipante == null && other.idparticipante != null) || (this.idparticipante != null && !this.idparticipante.equals(other.idparticipante))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.lania.sai.entidades.Participantes[ idparticipante=" + idparticipante + " ]";
    }
    
}
