package mx.lania.sai.oad;

import java.util.List;
import mx.lania.sai.entidades.Agentecontratante;

/**
 *
 * @author jaguilar
 */
public interface OadAgentecontratante {
    
    void crear(Agentecontratante agente);
    
    void actualizar(Agentecontratante agente);
    
    List<Agentecontratante> buscarPorNombre(String nombre);

    //List<Agentecontratante> buscarPorId(int idAgente);
    
    List<Agentecontratante> findAll();
    
    Agentecontratante getPorId(Integer id);
    
}
