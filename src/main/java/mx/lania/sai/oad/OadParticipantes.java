package mx.lania.sai.oad;

import java.util.List;
import mx.lania.sai.entidades.Participantes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author PCEL
 */
public interface OadParticipantes extends JpaRepository<Participantes, Integer> {
    
    @Query("SELECT e FROM Participante e WHERE e.idParticipante = :idParticipante")
    List<Participantes> getPorIdDepartamento(@Param("idParticipante") Integer idParticipante);

    public List<Participantes> findByNombreParticipante(String nombre);
}
